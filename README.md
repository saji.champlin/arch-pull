# arch-pull: pull-mode configuration for clients

This repo is pulled frequently by Arch Linux clients to automatically provision and configure them.
Sensitive information is stored in vaults.

## Bootstrapping
This system is designed to be bootstrapped as early as possible (First boot, minimal installed packages).
As a result, it can feel unwieldy to already provisioned systems. Due to ansible's idempotent nature, it *should*
work nicely out of the box, but some adjustments may have to be made before it is green across the board.

The general outline for bootstrap is as follows (from fresh install):
- `pacman -Sy ansible ansible-base curl`
- Set vault env variable: `export VAULT_PASS=<vault password>`
- get vault loader variable: `curl -o get_pass.sh 'https://gitlab.com/saji.champlin/arch-pull/-/raw/master/get_pass.sh'`. (or just make it)
- `chmod +x get_pass.sh`
- `ansible-pull -U https://gitlab.com/saji.champlin/arch-pull.git arch.yml --vault-password-file ./get_pass.sh`

At this point the system will begin the bootstrap. This will take a while!
