This role installs avahi and configures systemd-resolved to not use MDNS.
This is required for cups.
