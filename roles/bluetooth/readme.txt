This role installs the bluetooth stack. It also adds bluetooth audio support
with pulseaudio-bluetooth (which should technically go in sound).
