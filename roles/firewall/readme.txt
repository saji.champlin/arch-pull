installs firewalld and management tools as well as nftables replacements
for network tools (iptables, ebtables, etc). This is so that it follows
the modern network stack rather than old iptables.
