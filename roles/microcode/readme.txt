installs microcode for the processor on the system. This does not configure
the bootloader to use said microcode before booting the kernel. That is
left to manual configuration for protection.
