installs networkmanager and related programs to handle the network stack.
This can be considered the "network" role, as it also handles dnsmasq
and dhclient.
