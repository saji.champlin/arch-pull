configures hibernation and lid behavior for the system. It also includes
a host-specific hook for the apartment network which has issues recovering from sleep.
